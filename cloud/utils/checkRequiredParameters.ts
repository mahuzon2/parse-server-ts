function checkRequiredParameter(requiredParams:string[], reqParams:any) {

  for (const requiredParam of requiredParams) {
    if (!reqParams[requiredParam]) {
      throw new Parse.Error(
        Parse.Error.ACCOUNT_ALREADY_LINKED,
        `missing the required parameter ${requiredParam}`
      );
    }
    if (requiredParam === "userToken") {
      //   realm = checkUserToken(reqParams.userToken);
    }
  }

  //   return { realm: realm, formatedRealm: formatString(realm) };
}
export default checkRequiredParameter;
