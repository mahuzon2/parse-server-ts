export default function checkRequiredParameters(
  paramsList: string[],
  reqParams: { [x: string]: any }
) {
  for (let p of paramsList) {
    if (!reqParams[p]) {
      throw new Parse.Error(400, `missing parameter ${p}`);
    }
  }
}
