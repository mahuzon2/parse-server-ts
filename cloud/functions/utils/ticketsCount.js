const _ = require("lodash");
async function ticketsCount(label, startDate, endDate) {
  let query = new Parse.Query("Tickets");
  // query.equalTo(label, value);

  if (startDate || endDate) {
    if (startDate) query.greaterThanOrEqualTo("updatedAt", new Date(startDate));
    if (endDate) {
      let ed = new Date(endDate);
      ed.setHours(23, 59, 59, 999);
      query.lessThanOrEqualTo("updatedAt", ed);
    }
  } else {
    let today = new Date();

    today.setHours(0, 0, 0, 0);
    query.greaterThanOrEqualTo("updatedAt", today);

    today.setHours(23, 59, 59, 999);
    query.greaterThanOrEqualTo("updatedAt", today);
  }

  let smartQueues = await query.findAll();

  // get the
  let labels = smartQueues.map((it) => it.attributes[label]);
  // let uniqLables = _.uniq(labels);

  let count = _.countBy(labels, (lab) => lab);

  return { [`${label}s`]: count, total: Object.keys(count).length };
}

module.exports = ticketsCount;
