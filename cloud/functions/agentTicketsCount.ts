import checkRequiredParameters from "./utils/checkRequiredParameter";
const ticketsCount = require("./utils/ticketsCount");

Parse.Cloud.define("agentTicketsCount", async (req) => {
  checkRequiredParameters(["agentId"], req.params);

  let agentId = req.params.agentId;
  let startDate = req.params.startDate;
  let endDate = req.params.endDate;

  return await ticketsCount("agent", startDate, endDate);
});
