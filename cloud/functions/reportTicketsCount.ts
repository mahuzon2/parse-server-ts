Parse.Cloud.define("reportTicketsCount", async (req) => {
  let startDate = req.params.startDate;
  let endDate = req.params.endDate;

  let waiting = 0;
  let attending = 0;
  let onhold = 0;
  let noshow = 0;
  let closed = 0;
  let transferred = 0;

  let query = new Parse.Query("Tickets");

  if (startDate || endDate) {
    if (startDate) query.greaterThanOrEqualTo("updatedAt", new Date(startDate));
    if (endDate) {
      let ed = new Date(endDate);
      ed.setHours(23, 59, 59, 999);
      query.lessThanOrEqualTo("updatedAt", ed);
    }
  } else {
    let today = new Date();

    today.setHours(0, 0, 0, 0);
    query.greaterThanOrEqualTo("updatedAt", today);

    today.setHours(23, 59, 59, 999);
    query.greaterThanOrEqualTo("updatedAt", today);
  }

  let tickets = await query.findAll();

  if (tickets.length) {
    for (let ticket of tickets) {
      if (ticket.attributes.status === "CREATED") {
        waiting++;
      } else if (ticket.attributes.status === "ATTENDING") {
        attending++;
      } else if (ticket.attributes.status === "ONHOLD") {
        onhold++;
      } else if (ticket.attributes.status === "NOSHOW") {
        noshow++;
      } else if (ticket.attributes.status === "CLOSED") {
        closed++;
        if (ticket.attributes.transferredToUser) transferred++;
      }
    }
  }

  return {
    total: tickets.length,
    waiting,
    attending,
    onhold,
    noshow,
    closed,
    transferred,
    tickets,
  };
});
