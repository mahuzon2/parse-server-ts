const checkRequiredParameter = require("../utils/checkRequiredParameters");

Parse.Cloud.define("mapSmartQueusToGroup", async (req:any) => {
  checkRequiredParameter(["groupId", "companyId", "token"], req.params);
  // req.params = {
  //     groupId: String,
  //     addTo: String[],
  //     rmFrom: String[],
  //     companyId: String,
  //     token: String

  const companyId = req.params.companyId;
  const groupId = req.params.groupId;
  const addTo = req.params.addTo;
  const rmFrom = req.params.rmFrom;

  let SmartQueus = Parse.Object.extend(companyId + "_SmartQueus");

  const group = new Parse.Object("Group");
  group.id = groupId;

  for (let id of addTo) {
    let sq = new SmartQueus();
    sq.id = id;
    sq.set({ group });
    await sq.save();
  }

  for (let id of rmFrom) {
    let sq = new SmartQueus();
    sq.id = id;
    sq.set("group", null);
    await sq.save();
  }

  return true;
});
