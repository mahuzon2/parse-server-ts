import express ,{Express} from "express"
import parseServer from "./parse/parse-api";
import dashboard from "./parse/parse-dash";
let app: Express = express();

parseServer(app, "/rest");
dashboard(app, "/dash");

let httpServer = require("http").createServer(app);
httpServer.listen(1337);