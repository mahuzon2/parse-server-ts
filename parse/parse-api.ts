import { Express } from "express";
const ParseServer = require("parse-server").ParseServer;

require("dotenv").config();

const { appId, appName, javascriptKey, masterKey, MONGODB_URI } = process.env;
let logger = console;

let api = new ParseServer({
  appId,
  appName,
  javascriptKey,
  masterKey,
  directAccess: true,
  enforcePrivateUsers: true,
  allowClientClassCreation: true,
  // databaseURI: 'postgres://localhost:5432/db', // url de connexion a la bdd postgres
  databaseURI: MONGODB_URI, // url de connexion a la bdd mongo
  port: 1337,
  mountPath: "/rest",
  serverStartComplete: () => logger.info("rightdesk backend started"),
  serverCloseComplete: () => logger.info("rightdesk backend closed"),
  enableAnonymousUsers: true,
  preventLoginWithUnverifiedEmail: true,
  cloud: "./cloud/index.ts",
  allowPublic: true,
  fileUpload: { enableForPublic: true },
  // logger: logger,
  // verbose: true,
  // loggerAdapter: logger,
});

async function parseServer(app: Express, path: string) {
  await api.start();
  app.use(path, api.app);
}

export default parseServer;
