// import  ParseDashboard from "parse-dashboard";
import { Express } from "express";
const ParseDashboard = require("parse-dashboard");

const { appId, appName, javascriptKey, masterKey, SERVER_URL } = process.env;

var options = { allowInsecureHTTP: true };

var dashboard = new ParseDashboard(
  {
    apps: [
      {
        serverURL: process.env.SERVER_URL,
        // "serverURL": "https://rightdesk-backend.rightcomtech.com/rest",
        appId,
        masterKey,
        javascriptKey,
        appName,
      },
    ],
    users: [
      {
        user: "admin",
        pass: "rqb",
        apps: [{ appId }],
      },
    ],
  },
  options
);

function parseDashboard(app: Express, path: string) {
  app.use(path, dashboard);
}

export default parseDashboard;
