import Parse from "parse/node";
const appId = "rightq";
const appName = "RightQ";
const javascriptKey = "javascriptKey";
const masterKey = "masterKey";
const serverUrl = "http://localhost:1337/rest";
Parse.initialize(appId, javascriptKey, masterKey);
Parse.serverURL = serverUrl;

Parse.Cloud.run("mapSmartQueusToGroup", {
  groupId: "groupId",
  addTo: [],
  rmFrom: [],
  companyId: appId,
  token: "token",
})
  .then(console.log)
  .catch(console.log);
